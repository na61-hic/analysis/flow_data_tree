#
# Pid
#
set(PID_GIT "https://gitlab.cern.ch/na61-hic/pid.git" CACHE STRING "Path to PID GIT")

set(Pid_INSTALL_DIR ${EXTERNAL_INSTALL_DIR})
set(Pid_INCLUDE_DIR ${Pid_INSTALL_DIR}/include)
set(Pid_LIBRARY_DIR ${Pid_INSTALL_DIR}/lib)

ExternalProject_Add(Pid_Ext
        GIT_REPOSITORY  ${PID_GIT}
        GIT_TAG         "master"
        UPDATE_DISCONNECTED ${UPDATE_DISCONNECTED}
#        UPDATE_COMMAND  "${EXTERNAL_UPDATE_COMMAND}"
        SOURCE_DIR      "${EXTERNAL_DIR}/Pid_src"
        BINARY_DIR      "${EXTERNAL_DIR}/Pid_build"
        INSTALL_DIR     "${Pid_INSTALL_DIR}"
#        CONFIGURE_COMMAND ""
#        BUILD_COMMAND     ""
#        INSTALL_COMMAND   ""
#        TEST_COMMAND      ""
        CMAKE_ARGS
            "-DCMAKE_INSTALL_PREFIX=${Pid_INSTALL_DIR}"
            "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
)


list(APPEND PROJECT_DEPENDENCIES Pid_Ext)
list(APPEND PROJECT_LINK_DIRECTORIES ${Pid_LIBRARY_DIR})
list(APPEND PROJECT_INCLUDE_DIRECTORIES ${Pid_INCLUDE_DIR})
