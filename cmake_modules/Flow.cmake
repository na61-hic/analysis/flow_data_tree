#
# DataTreeQA
#

set(Flow_INSTALL_DIR ${EXTERNAL_INSTALL_DIR})
set(Flow_INCLUDE_DIR ${Flow_INSTALL_DIR}/include)
set(Flow_LIBRARY_DIR ${Flow_INSTALL_DIR}/lib)

ExternalProject_Add(Flow_Ext
        GIT_REPOSITORY  "https://github.com/eugene274/flow.git"
        GIT_TAG         "master"
        UPDATE_DISCONNECTED ${UPDATE_DISCONNECTED}
#        UPDATE_COMMAND  "${EXTERNAL_UPDATE_COMMAND}"
        SOURCE_DIR      "${EXTERNAL_DIR}/Flow_src"
        BINARY_DIR      "${EXTERNAL_DIR}/Flow_build"
        INSTALL_DIR     "${Flow_INSTALL_DIR}"
#        CONFIGURE_COMMAND "DATATREE_HOME=${DataTree_INSTALL_DIR} ${CMAKE_COMMAND}"
#        BUILD_COMMAND     ""
#        INSTALL_COMMAND   ""
#        TEST_COMMAND      ""
        CMAKE_ARGS
            "-DCMAKE_INSTALL_PREFIX=${Flow_INSTALL_DIR}"
            "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
            "-DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}"
)


list(APPEND PROJECT_DEPENDENCIES Flow_Ext)
list(APPEND PROJECT_LINK_DIRECTORIES ${Flow_LIBRARY_DIR})
list(APPEND PROJECT_INCLUDE_DIRECTORIES ${Flow_INCLUDE_DIR})

