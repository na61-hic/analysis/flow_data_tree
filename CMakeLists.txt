cmake_minimum_required(VERSION 3.0.0)
project(DataTreeFlow)

include(ExternalProject)

set(CMAKE_CXX_STANDARD 11)

set(UPDATE_DISCONNECTED FALSE CACHE STRING "Skip update step")
#set(REBUILD_EXTERALS TRUE CACHE STRING "Rebuild external projects every time")
SET(DATATREE_PATH       "" CACHE STRING "Path to DataTree")
SET(DATATREEQA_PATH     "" CACHE STRING "Path to DataTreeQA")
SET(CENTRALITY_PATH     "" CACHE STRING "Path to Centrality")
SET(PID_PATH            "" CACHE STRING "Path to Pid")
SET(QN_PATH             "" CACHE STRING "Path to Flow")

#if(REBUILD_EXTERALS)
#  set(EXTERNAL_UPDATE_COMMAND "${CMAKE_COMMAND} --build . install")
#else()
#  set(EXTERNAL_UPDATE_COMMAND "")
#endif()

set(EXTERNAL_DIR ${CMAKE_BINARY_DIR}/external)
set(EXTERNAL_INSTALL_DIR ${EXTERNAL_DIR}/install)
set(EXPERIMENT "SHINE" CACHE STRING "Select experiment SHINE/CBM")

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'release' as none was specified.")
  set(CMAKE_BUILD_TYPE "release")
endif()

# ROOT
list(APPEND CMAKE_PREFIX_PATH "$ENV{ROOTSYS}")
find_package(ROOT REQUIRED)
# include(${ROOT_USE_FILE})

# Boost
find_package(Boost REQUIRED COMPONENTS program_options)

list(APPEND CMAKE_PREFIX_PATH ${DATATREE_PATH})
list(APPEND CMAKE_PREFIX_PATH ${DATATREEQA_PATH})
list(APPEND CMAKE_PREFIX_PATH ${CENTRALITY_PATH})
list(APPEND CMAKE_PREFIX_PATH ${PID_PATH})
list(APPEND CMAKE_PREFIX_PATH ${QN_PATH})
list(APPEND CMAKE_PREFIX_PATH $ENV{DATATREE_PATH})
list(APPEND CMAKE_PREFIX_PATH $ENV{DATATREEQA_PATH})
list(APPEND CMAKE_PREFIX_PATH $ENV{CENTRALITY_PATH})
list(APPEND CMAKE_PREFIX_PATH $ENV{PID_PATH})
list(APPEND CMAKE_PREFIX_PATH $ENV{QN_PATH})

include(cmake_modules/DataTree.cmake)
include(cmake_modules/DataTreeQA.cmake)
include(cmake_modules/pid.cmake)
include(cmake_modules/centrality.cmake)
include(cmake_modules/Flow.cmake)

link_directories(${PROJECT_LINK_DIRECTORIES})
include_directories(${PROJECT_INCLUDE_DIRECTORIES})


add_library(config STATIC src/GlobalConfig.h src/GlobalConfig.cpp)
add_dependencies(config ${PROJECT_DEPENDENCIES})
target_link_libraries(config
	PUBLIC
		Pid
		Centrality
		DataTree
		DataTreeQA
		DataTreeCuts
	PRIVATE
		ROOT::Core
		ROOT::RIO
		ROOT::Physics
		ROOT::EG
		ROOT::Tree
	
		Boost::program_options
	)

add_executable(correct
        src/Correct.cpp
        src/CorrectionTask.cpp
        src/DataTreeVarManager.cpp
        src/Utils.cpp
        )
add_dependencies(correct ${PROJECT_DEPENDENCIES})
target_link_libraries(correct
	PRIVATE 
		QnCorrections 
		Correction 
		Base
		
		config 
		
		ROOT::Core
		ROOT::RIO
		ROOT::Physics
		ROOT::EG
		ROOT::TreePlayer
		)
target_compile_definitions(correct PUBLIC "-DDATATREE_${EXPERIMENT}")
target_compile_options(correct PRIVATE -Wall)
	

add_executable(analysis src/Analysis.cpp src/CorrelationTask.cpp src/CorrelationFct.cpp)
add_dependencies(analysis ${PROJECT_DEPENDENCIES})
target_link_libraries(analysis
	PRIVATE
		Base
		Correlation

		config 
		
		ROOT::Core
		ROOT::RIO
		ROOT::Physics
		ROOT::EG
		ROOT::TreePlayer
		)
target_compile_options(analysis PRIVATE -Wall)


#add_executable(TestCorrelatedErrors test/errors/TestCorrelatedErrors.cpp )
#add_dependencies(TestCorrelatedErrors ${PROJECT_DEPENDENCIES})
#target_link_libraries(TestCorrelatedErrors  QnCorrections Correction Base Correlation ${ROOT_LIBRARIES})


# add_subdirectory(src/tools)


