//
// Created by eugene on 06/12/2019.
//

#include <cassert>
#include "CorrelationFct.h"

CorrelationFct CorrelationFct::fromString(const std::string &str) {
  CorrelationFct fct;
  fct.fName = str;

  CorrelationFct::Correlant correlant;
  bool isAxisFilled = false;
  for (auto iter = str.cbegin(); iter != str.cend(); ++iter) {
    char c = *iter;
    bool isX = c == 'x' || c == 'X';
    bool isY = c == 'Y' || c == 'y';
    bool isXY = isX || isY;
    bool isLast = iter == str.end() - 1;
    if (isXY) {
      if (isAxisFilled) {
        /* new correlant to be added with default harmonic */
        correlant.fHarmonic = 1;
        fct.fCorrelants.push_back(correlant);
        isAxisFilled = false;
      }
      correlant.fAx = isX? EAxis::kX : EAxis::kY;
      isAxisFilled = true;
      if (isLast) {
        fct.fCorrelants.push_back(correlant);
        /* the end */
      }
    } else if (isdigit(c)) {
      assert(isAxisFilled);
      /* new correlant with given harmonic */
      correlant.fHarmonic = c - '0';
      fct.fCorrelants.push_back(correlant);
      isAxisFilled = false;
    } else {
      throw std::runtime_error("bad format");
    }
  }

  return fct;
}

std::string CorrelationFct::fname() const {
  std::string result;
  result.append(std::to_string(factor));
  for (const Correlant &correlant : fCorrelants) {
    result.append(correlant.fAx == EAxis::kX? "X" : "Y");
    result.append(std::to_string(correlant.fHarmonic));
  }

  if (fNormalization == ENormalization::kMagnitude) {
    result.append("_n");
  }

  return result;
}

double CorrelationFct::operator()(const std::vector<Qn::QVectorPtr> &a) {
  assert(a.size() == fCorrelants.size());

  double result = factor;
  for (size_t iArg = 0; iArg < fCorrelants.size(); ++iArg) {
    const Correlant &c = fCorrelants[iArg];
    result *= (c.fAx == EAxis::kX)? a[iArg].x(c.fHarmonic) : a[iArg].y(c.fHarmonic);

    if (fNormalization == ENormalization::kMagnitude) {
      result /= a[iArg].mag(c.fHarmonic);
    } else if (fNormalization == ENormalization::kNone) {
      /* do nothing */
    }
  }
  return result;
}

CorrelationFct CorrelationFct::normalized() {
  CorrelationFct newCF(*this);
  /* already normalized */
  if (newCF.fNormalization == ENormalization::kMagnitude) {
    return newCF;
  }

  newCF.fNormalization = ENormalization::kMagnitude;
  newCF.fName.append("n");
  return newCF;
}

CorrelationFct CorrelationFct::operator*(const double f) {
  CorrelationFct newCF(*this);
  newCF.factor *= f;
  return newCF;
}
