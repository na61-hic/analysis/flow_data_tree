#include <utility>
#include <cstring>
#include <string>
#include <map>
#include <TFileCollection.h>

//
// Created by Lukas Kreis on 13.11.17.
//

#include "CorrelationTask.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TTreeReaderValue.h"

#include "GlobalConfig.h"
#include "CorrelationFct.h"

CorrelationTask::CorrelationTask(std::string filelist, std::string treename) :
    inputTree_(CorrelationTask::MakeChain(std::move(filelist), std::move(treename))) {}

void CorrelationTask::Configure(Qn::CorrelationManager &a) {
  using std::vector;
  using std::pair;
  using std::string;

  using mag = CorrelationFct::mag;

  const vector<CorrelationFct> cf2d {
	  "XY"_cf*2,
	  "YX"_cf*2,
	  "XX"_cf*2,
	  "YY"_cf*2,
	  /* normalized correlations for EP method */
      "XY"_cf*2/mag(),
      "YX"_cf*2/mag(),
      "XX"_cf*2/mag(),
      "YY"_cf*2/mag(),
  };

  std::cout << "2c correlations:" << std::endl;
  std::for_each(cf2d.begin(), cf2d.end(), [] (const CorrelationFct &fct) { std::cout << "\t" << fct.name() << "(" << fct.fname() << ")" << std::endl; });

  const vector<CorrelationFct> cf3d {
  	  /* non-zero correlations */
	  "Y2XY"_cf,
  	  "Y2YX"_cf,
  	  "X2XX"_cf,
  	  "X2YY"_cf,

	  /* zero correlations */
	  "X2XY"_cf,
	  "X2YX"_cf,
	  "Y2XX"_cf,
	  "Y2YY"_cf,
    };

  std::cout << "3c correlations:" << std::endl;
  std::for_each(cf3d.begin(), cf3d.end(), [] (const CorrelationFct &fct) { std::cout << "\t" << fct.name() << "(" << fct.fname() << ")" << std::endl; });

  a.SetOutputFile("corr.root");

  const std::vector<std::string> detectors{
      "psd1", "psd2", "psd3",

      "v0_lambda_pT", "v0_lambda_y",

      "proton_pT", "proton_y",

      "pion_pT", "pion_y",

      "pion_neg_pT", "pion_neg_y",
  };


  const std::vector<std::string> detectorsPSD{detectors.begin(), detectors.begin() + 3};
  const std::vector<std::string> detectorsTracks{detectors.begin() + 3, detectors.end()};

  auto stringify = [](const std::vector<std::string> &detectors) {
    const std::string del{", "};
    std::ostringstream detectorsStringStream{};
    std::copy(detectors.begin(), detectors.end(), std::ostream_iterator<std::string>(detectorsStringStream, del.c_str()));
    std::string result_raw{detectorsStringStream.str()};
    return std::string{result_raw.begin(), result_raw.end() - del.length()};
  };

  Info(__func__, "Detectors: [%s]", stringify(detectors).c_str());
  Info(__func__, "Track Detectors: [%s]", stringify(detectorsTracks).c_str());
  Info(__func__, "PSD Detectors: [%s]", stringify(detectorsPSD).c_str());

  a.AddEventAxis({"Centrality", {0, 5, 10, 15, 25, 35, 45, 60, 80, 100.}});

  const std::string detectorsMC[] = {
      "mc_proton_pT", "mc_proton_y",
      "mc_lambda_pT", "mc_lambda_y",
      "mc_pion_pT", "mc_pion_y",
      "mc_pion_neg_pT", "mc_pion_neg_y",
  };

  const vector<vector<string> > vRefVsRefCorrelation{
      {"psd1", "psd2"},
      {"psd2", "psd3"},
      {"psd3", "psd1"},
  };

  const vector<pair<string,string> > vMCRefVsRefCorrelation{
        {"mc_psd1", "mc_psd2"},
        {"mc_psd2", "mc_psd3"},
        {"mc_psd3", "mc_psd1"},
    };

  const std::string sPsdPsd[] = {"psd1_psd2", "psd2_psd3", "psd3_psd1",};

  const std::string sPsdPsdPsd = "psd1_psd2_psd3";
  const std::string sPsdPsdPsdName = "psd1, psd2, psd3";

  a.SetResampling(samplingMode, nSamples);

  /**
   * Correlations of all detectors vs PsiRP
   */
  if (isSimulation) {
	/* detectors vs PSI_RP  */
    for (const auto &det : detectors) {
      vector<string> correlationDetectors{det, "psi"};
      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};

      for (const CorrelationFct &fct : cf2d)
    	  a.AddCorrelation(det + "_psi_" + fct.name(), correlationDetectors, fct, correlationWeights);
    }

    /* MC detectors vs PSI_RP  */
    for (const auto &iDet : detectorsMC) {
      vector<string> correlationDetectors{iDet, "psi"};
      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};

      for (const CorrelationFct &fct : cf2d)
    	  a.AddCorrelation(iDet + "_psi_" + fct.name(), correlationDetectors, fct, correlationWeights);
    }

    /* MC PSD with each other */
    for (const pair<string,string> &mcPsdPair : vMCRefVsRefCorrelation) {
    	vector<string> correlationDetectors{mcPsdPair.first, mcPsdPair.second};
    	vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};
    	for (const CorrelationFct &fct : cf2d)
    	    	  a.AddCorrelation(mcPsdPair.first + "_" + mcPsdPair.second + "_" + fct.name(), correlationDetectors, fct, correlationWeights);
    }

  }

  /**
   * Correlations of tracks vs PSD
   */
  for (const auto &psd : detectorsPSD) {
    for (const auto &track : detectorsTracks) {
        for (const CorrelationFct &fct : cf2d)
        	a.AddCorrelation(track + "_" + psd + "_" + fct.name(), {track, psd}, fct, {Qn::Weight::OBSERVABLE, Qn::Weight::REFERENCE});
    }
  }

  for (ushort iDet = 0; iDet < 3; ++iDet) {
	  /* Correlations between PSDs */
	  for (const CorrelationFct &fct : cf2d)
		  a.AddCorrelation(sPsdPsd[iDet] + "_" + fct.name(), vRefVsRefCorrelation[iDet], fct, {Qn::Weight::REFERENCE, Qn::Weight::REFERENCE});

	  /* correlations of tracks with PSD-PSD pairs (for v2 and MH) */
    for (const auto &sTrack : detectorsTracks) {
      vector<string> correlationDetectors;
      correlationDetectors.push_back(sTrack);
      correlationDetectors.insert(correlationDetectors.end(), vRefVsRefCorrelation[iDet].begin(), vRefVsRefCorrelation[iDet].end());

      vector<Qn::Weight> correlationWeights{Qn::Weight::REFERENCE, Qn::Weight::REFERENCE, Qn::Weight::REFERENCE};

      for (const CorrelationFct &fct : cf3d)
    	  a.AddCorrelation(sTrack + "_" + sPsdPsd[iDet] + "_" + fct.name(), correlationDetectors, fct, correlationWeights);
    }
  }

}

void CorrelationTask::Run() {
  Qn::CorrelationManager a(inputTree_.get());
  a.EnableDebug();
  Configure(a);
  std::cout << "CorrelationManager::Run()..." << std::endl;
  a.Run();
  std::cout << "Done." << std::endl;
}

std::unique_ptr<TChain> CorrelationTask::MakeChain(const std::string& filename, const std::string& treename) {
  std::cout << "Adding files to chain:" << std::endl;
  std::unique_ptr<TChain> chain(new TChain(treename.c_str()));
  if (filename.rfind(".root") < filename.size()) {
    chain->AddFile(filename.data());
  }
  else {
    TFileCollection fc("fc", "", filename.c_str());
    chain->AddFileInfoList(reinterpret_cast<TCollection *>(fc.GetList()));
  }
  chain->ls();
  return chain;
}

void CorrelationTask::ReadConfig() {

  const auto &globalConfig = *gConfig;

  isSimulation = globalConfig.IsSimulation();
  Info(__func__, "%s", isSimulation ? "MC mode" : "Reco mode");

  if (globalConfig.getSamplingMode() != "NONE") {
    if (globalConfig.getSamplingMode() == "SUBSAMPLING") {
      samplingMode = Qn::Sampler::Method::SUBSAMPLING;
    } else if (globalConfig.getSamplingMode() == "BOOTSTRAP") {
      samplingMode = Qn::Sampler::Method::BOOTSTRAP;
    } else {
      throw std::logic_error(globalConfig.getSamplingMode() + " is not known");
    }

    nSamples = globalConfig.getNSamples();
    Info(__func__, "Sampling Mode %s(%d)", globalConfig.getSamplingMode().c_str(), nSamples);
  }
}
