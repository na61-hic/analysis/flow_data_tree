//
// Created by eugene on 3/28/19.
//


#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include "GlobalConfig.h"

bool RequireOption(const boost::program_options::variables_map &vm, const std::string &name) {
    if (!vm.count(name)) {
      throw std::runtime_error("Option '" + name + "' is required.");
    }

    return true;
}

bool GlobalConfig::LoadIni(const char *filename) {
  Info(__func__, "Loading '%s'", filename);
  using std::string;
  namespace po = boost::program_options;

  const bool allow_unregistered = true;

  po::options_description common_opts;
  common_opts.add_options()
      ("nPsdModules", po::value<int>(&nPsdModules)->required())
      ("psdLayoutName", po::value<string>(&psdSubeventLayoutName)->required())
      ("isSimulation", po::value<bool>(&isSimulation)->default_value(false));

  po::options_description cuts_opts;
  cuts_opts.add_options()
      ("cuts.configFile", po::value<string>(&configFileName)->required())
      ("cuts.configName", po::value<string>(&configName)->required())
      ("cuts.cutsConfig", po::value<string>(&cutsConfigName)->required());

  po::options_description centrality_opts;
  centrality_opts.add_options()
      ("centrality.centralityFile", po::value<string>(&centralityFileName)->required())
      ("centrality.centralityGetterName", po::value<string>(&centralityGetterName)->default_value("centr_getter_1D"))
      ("centrality.centralityVariable", po::value<string>(&centralityVariable)->required())
      ;

  po::options_description pid_opts;
  pid_opts.add_options()
      ("pid.pidSource", po::value<string>(&pidSourceStr)->default_value("file"))
      ("pid.pidFile", po::value<string>(&pidFileName))
      ("pid.pidGetterName", po::value<string>(&pidGetterName)->default_value("pid_getter"))
      ("pid.pidVariable", po::value<string>(&pidVariable))
      ("pid.pidPurity", po::value<double>(&pidPurity))
      ;

  po::options_description sampling_opts;
  sampling_opts.add_options()
      ("sampling.samplingMode", po::value<string>(&samplingMode)->default_value("NONE"))
      ("sampling.nSamples", po::value<int>(&nSamples)->default_value(1));

  po::options_description psd_subevents_opts;
  for (const string &det : {"psd1", "psd2", "psd3"}) {
    po::options_description specific_subevent_opts;
    specific_subevent_opts.add_options()
        ((det + "." + "correctionSteps").c_str(), po::value<int>()->default_value(PSD_CORRECTION_STEPS_DEFAULT));

    psd_subevents_opts.add(specific_subevent_opts);
  }

  po::options_description all;
  all
      .add(common_opts)
      .add(cuts_opts)
      .add(centrality_opts)
      .add(pid_opts)
      .add(sampling_opts)
      .add(psd_subevents_opts);

  po::variables_map vm;

  std::ifstream config_fd(filename);
  po::store(po::parse_config_file(config_fd, all, allow_unregistered), vm);

  try {
    po::notify(vm);
  } catch (const po::error &e) {
    Error(__func__, "%s", e.what());
    return false;
  }

  auto GetOptInt = [vm](const string &optName, int defval) {
    if (vm.count(optName)) {
      return vm[optName].as<int>();
    }
    return defval;
  };


  for (const string &det : {"psd1", "psd2", "psd3"}) {
    auto correctionSteps = GetOptInt(det + "." + "correctionSteps", PSD_CORRECTION_STEPS_DEFAULT);
    psdSubeventsCorrectionSteps.emplace(det, correctionSteps);
  }

  // PID initialization
  if (pidSourceStr == "file") {
    pidSource = EPidSource::kFile;
    RequireOption(vm, "pid.pidFile") && RequireOption(vm, "pid.pidVariable") && RequireOption(vm, "pid.pidPurity");
    LoadResource(pidFileName, pidGetterName, pidGetter);
  } else if (pidSourceStr == "matching") {
    pidSource = EPidSource::kMatching;
  } else if (pidSourceStr == "variable") {
    pidSource = EPidSource::kVariable;
    RequireOption(vm, "pid.pidVariable");
  } else {
    throw std::runtime_error("Unknown PID source: " + pidSourceStr);
  }

  // Centrality

  isInitialized = true;
  return true;
}

bool GlobalConfig::LoadResources() {

  LoadResource(configFileName, configName, qaConfig);

  if (cutsConfigName.empty()) {
    cuts::DataTreeCutsConfig *cutsConfigRaw{nullptr};
    cutsConfigRaw = qaConfig->GetCutsConfig();

    if (cutsConfigRaw) {
      cutsConfig = std::shared_ptr<cuts::DataTreeCutsConfig>(cutsConfigRaw);
    } else {
      DieWith("no cuts config");
      return false;
    }
  } else {
    LoadResource(configFileName, cutsConfigName, cutsConfig);
  }

  LoadResource(centralityFileName, centralityGetterName, centralityGetter);


  return true;
}
