//
// Created by eugene on 9/9/19.
//

#ifndef DATATREEFLOW_CUTS_H
#define DATATREEFLOW_CUTS_H

#include <functional>
#include <TMath.h>

enum class ETrackType : int {
  kVertexTrack = 0,
  kMCTrack,
  kV0Track
};

namespace flow {

namespace cuts {

template<ETrackType trackType>
bool type(double tt) {
  return static_cast<int>(trackType) == TMath::Nint(tt);
}

inline std::function<bool(double)> range(double lo, double hi) {
  return [=](double x) { return lo <= x && x <= hi; };
}

inline std::function<bool(double)> rangeStrict(double lo, double hi) {
  return [=](double x) { return lo < x && x < hi; };
}

template<int p>
bool pid(double _pid) {
  return TMath::Nint(_pid) == p;
}

inline std::function<bool(double)> pid(int pid) {
  return [=](double _pid) { return TMath::Nint(_pid) == pid; };
}

inline bool positive (double x) {
	return x > 0;
}

inline bool negative (double x) {
	return x < 0;
}



/*
 * Specific cuts e.g PSD acceptance
 * todo(EK) consider make a named cuts?
 */
constexpr double distanceToPSD = 18; // 591.9 + 12 ~ 18 meters

inline bool inAcceptancePSD1NA61(double px, double py, double pz) {
	if (
			pz > 0 &&
			TMath::Abs(px/pz*distanceToPSD) <= 0.2 &&
			TMath::Abs(py/pz*distanceToPSD) <= 0.2
	) return true;

	return false;
}

inline bool inAcceptancePSD2NA61(double px, double py, double pz) {
	if (
			!inAcceptancePSD1NA61(px, py, pz) &&
			pz > 0 &&
			TMath::Abs(px/pz*distanceToPSD) <= 0.4 &&
			TMath::Abs(py/pz*distanceToPSD) <= 0.4
	) return true;

	return false;
}

inline bool inAcceptancePSD3NA61(double px, double py, double pz) {
	if (
			!inAcceptancePSD1NA61(px, py, pz) &&
			!inAcceptancePSD2NA61(px, py, pz) &&
			pz > 0 &&
			TMath::Abs(px/pz*distanceToPSD) <= 0.6 &&
			TMath::Abs(py/pz*distanceToPSD) <= 0.6
	) return true;

	return false;
}

} // namespace cuts

} // namespace flow

#endif //DATATREEFLOW_CUTS_H
