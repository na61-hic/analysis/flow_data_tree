//
// Created by Lukas Kreis on 29.06.17.
//

#ifndef CORRECTION_TASK_H
#define CORRECTION_TASK_H


#include <vector>
#include <array>
#include <random>
#include <memory>
#include <TChain.h>

#include "TFile.h"
#include "TTree.h"
#include "TTreeReader.h"

#include <cuts/DataTreeCuts.h>

#include "CorrectionManager.h"
#include "DataTreeVarManager.h"

#define VAR DataTreeVarManager

namespace Qn {
/**
 * Qn vector analysis TestTask. It is to be configured by the user.
 * @brief TestTask for analysing qn vectors
 */
class CorrectionTask {
 public:
  CorrectionTask(const std::string &filelist, const std::string& incalib, const std::string &treename);
  ~CorrectionTask() = default;
  void Run();

  void ReadConfig();

 private:
  /**
   * Initializes TestTask;
   */
  void Initialize();
  /**
   * Processes one event;
   */
  void Process();
  /**
   * Finalizes TestTask. Called after processing is done.
   */
  void Finalize();
  /**
   * Make TChain from file list
   * @param filename name of file containing paths to root files containing the input trees
   * @return Pointer to the TChain
   */

  void AddQAHisto();

  static std::unique_ptr<TChain> MakeChain(const std::string& filename, const std::string& treename);


 protected:
  std::shared_ptr<TFile> out_file_;
  std::shared_ptr<TFile> in_calibration_file_;
  std::shared_ptr<TFile> out_calibration_file_;
  std::unique_ptr<TTree> in_tree_;
  TTree *out_tree_;
  TTreeReader tree_reader_;
  TTreeReaderValue<DataTreeEvent> event_;
  Qn::CorrectionManager manager_;

  std::shared_ptr<cuts::DataTreeCuts> cuts;

  std::shared_ptr<PSDSubeventLayout > psdSubeventLayout;


  unsigned int nGoodEvents_{0};
  unsigned int nTotalEvents_{0};

  TList *eventAndDetectorQA{nullptr};
};
}
#endif //CORRECTION_TASK_H
