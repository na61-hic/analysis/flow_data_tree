#include <iostream>
#include <iomanip>
#include <chrono>
#include <TApplication.h>

#include "CorrectionTask.h"

int main(int argc, char **argv) {
  using namespace std;

  string fileList = argv[1];
  string calibFileName = argv[2];
  string ini_file{argc > 3 ? argv[3] : "flow.ini"};
  if (gConfig->LoadIni(ini_file.c_str())) {
    gConfig->LoadResources();

    gVarManager->LoadConfig(*gConfig);

    Qn::CorrectionTask task(fileList, calibFileName, "DataTree");
    task.ReadConfig();

    std::cout << "go" << std::endl;
    auto start = std::chrono::system_clock::now();

    task.Run();

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << " s\n";
    return 0;
  }

  Error(__func__, "Invalid configuration");
  return 1;
}
