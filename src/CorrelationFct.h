//
// Created by eugene on 06/12/2019.
//

#ifndef DATATREEFLOW_SRC_CORRELATIONTASK_CPP_CORRELATIONFCT_H_
#define DATATREEFLOW_SRC_CORRELATIONTASK_CPP_CORRELATIONFCT_H_

#include <string>
#include <cstring>
#include <utility>
#include <vector>
#include <QVector.h>

class CorrelationFct {

public:
	enum class EAxis { kX, kY };
	enum class ENormalization { kNone, kMagnitude };

	struct mag {};

	struct Correlant { EAxis fAx; unsigned int fHarmonic; };

	/*
	 * format is
	 * X1Y1Y2 == XYY2 case-insensitive
	 */
	static CorrelationFct fromString(const std::string &str);

	std::string name() const { return fName; }

	std::string fname() const;

	double operator () (const std::vector<Qn::QVectorPtr> &a);

	CorrelationFct normalized();

	CorrelationFct operator /(mag /* m */) {
	  return this->normalized();
	}

	CorrelationFct operator *(double f);

private:
	CorrelationFct() = default;

	double factor{1.};
	ENormalization fNormalization{ENormalization::kNone};

	std::string fName{""};
	std::vector<Correlant> fCorrelants;

};

inline CorrelationFct operator "" _cf (const char *str, size_t size) {
	return CorrelationFct::fromString(std::string(str, size));
}

#endif //DATATREEFLOW_SRC_CORRELATIONTASK_CPP_CORRELATIONFCT_H_
